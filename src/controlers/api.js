// const my_domain = 'http://localhost:3003'
const my_domain = 'http://'+window.location.hostname+':3003'
// const my_domain = 'http://dds6.butik.ru/api'

module.exports = {
    getCategoryObj () {
        let page =''
        return fetch(my_domain+'/categories')
            .then(function(response) {
                page = response.json();
                return page;
            })
            .catch( function (e) {
                console.log('api error in '+my_domain+'/categories');
                console.log(e);
                return 'error'
            })
    },
    getCatalogListObj (category_id) {
        let page =''
        return fetch(my_domain+'/catalog/'+category_id)
        // return fetch(my_domain+'/products?category_id='+category_id)
            .then(function(response) {
                page = response.json();
                return page;
            })
            .catch( function (e) {
                console.log('api error in '+my_domain+'/catalog/'+category_id);
                console.log(e);
                return 'error'
            } )
    },
    getProductObj (productId) {
        let page =''
        return fetch(my_domain+'/product/'+productId)
            .then(function(response) {
                page = response.json();
                return page;
            })
            .catch( function (e) {
                console.log('api error in '+my_domain+'/catalog/'+productId);
                console.log(e);
                return 'error'
            } )
    }
};


