import React, { Component } from 'react';

const Categories_filter_generate_list = function (props) {
    let categories_block_onclick = props.categories_block_onclick;
    let tree = props.categories_tree;
    let obj = props.categories_obj;

    let result = [];
    let href_tree = tree.slice(0, 2);
    // let n = href_tree.length;

    for( let i in obj ){
        href_tree[0]=obj[i].name;
        let cur = href_tree.slice();
        cur.push( obj[i]._id )
        result.push(
            <div key={obj[i]._id} className={'category_filter_block'}>
                <a alt={ cur } onClick={ () => categories_block_onclick( cur ) } > { obj[i].name }</a>
            </div>
        );
        if (tree[2] === obj[i]._id )
            result.push(
                <div key={'s'+obj[i]._id} className={'category_filter_subcont'}>
                    <Recurs tree={tree} obj={ obj[i].subcategories } iterator={3} categories_block_onclick={categories_block_onclick} />
                </div>
            )
    }
    return result;

    function Recurs(props) {
        let result2 = [], iterator = props.iterator, tree = props.tree, active='active';
        iterator++
        let href_tree = tree.slice(0, iterator-1);
        
        let categories_block_onclick = props.categories_block_onclick;

        for( let i in props.obj ) {
            if (i==='0') { continue; }
            if ( props.obj[i]._id===tree[ tree.length-1 ] ) active='active'; else active='';
            
            href_tree[0]=props.obj[i].name;
            let cur = href_tree.slice();
            
            cur.push( props.obj[i]._id )
            result2.push( 
                <div key={props.obj[i]._id} className={'category_filter_subcont_block '+active}> 
                    <a alt={ cur } onClick={ () => categories_block_onclick( cur ) } > {props.obj[i].name}</a>
                </div> 
            );
            if (tree[iterator-1] === props.obj[i]._id ) 
                result2.push( 
                    <div key={'s'+props.obj[i]._id} className={'category_filter_subcont'}> 
                        <Recurs tree={tree} poi={ props.obj[i].subcategories } iterator={iterator} categories_block_onclick={categories_block_onclick} /> 
                    </div> 
                )
        }
        // console.log('END RECURS');
        return result2;
    }
}
class ProductListView extends Component {
    render() {
        console.log('PRODUCTS_list_view')
        let products_obj = this.props.products_obj;
        let prod_card_toggle = this.props.prod_card_toggle
        function get_row(obj1, obj2, obj3, key, func) {
            function ProductBlock(props) {
                let obj = props.product
                if (!obj.images) return <div>error</div>;
                return (
                    <div className={'product_block'}>
                        <div className={'image_container'} onClick={ ()=>func(obj) } >
                            <div><img src={obj.images[0] } alt=""/></div>
                        </div>
                        <span>{obj.name} {obj.brand_name}</span>
                        <span>{obj.price.toLocaleString('ru')} ₽</span>
                        <a>Заказать на примерку</a>
                    </div>
                )
            }
            return (
                <div key={key} className={'products_row'}>
                    <ProductBlock product={obj1}/>
                    <ProductBlock product={obj2}/>
                    <ProductBlock product={obj3}/>
                </div>
            )
        }
        let res = [], k=0, arr = products_obj.data.slice(), n = arr.length;
        if (n<1) return "грусть :((";
        for (let i in products_obj.data) {
            k++;
            if (typeof products_obj.data[i]!=='object') continue;
            if (k%3!==0) continue;
            res.push( get_row(products_obj.data[i-2], products_obj.data[i-1], products_obj.data[i], i , prod_card_toggle) )
        }
        res.push(<div key={'pagination'}> {products_obj.current_page} ... {products_obj.last_page}</div>)
        return res
    }
}

class ProductCardGen extends Component {
    render() {
        const product_cart_container_class = (this.props.product_card_obj.isactive) ? 'product_card_container' : 'hide'
        // const products_container_class = (!this.props.product_card_obj.isactive) ? 'products_container' : 'hide'
        // const info_line_class = (!this.props.product_card_obj.isactive) ? 'info_line' : 'hide'
        if (product_cart_container_class === 'hide') return true;
        let obj = this.props.product_card_obj.current_obj;
        console.log(obj)
        let arr=[]
        try{
            for ( let i in obj.images){
                arr.push( <div key={i}><img src={obj.images[i]} alt=""/></div> )
            }
        }catch (e){
            console.log('noooo')
            console.log(e)
        }

        let ImgList = ()=> {return arr}

        let arr2=[], SizeList
        if (obj.product_variations.length > 0)
        if (obj.product_variations[0].brand_size === 'б/р') SizeList = ()=> {return true}
        else{
            arr2.push(<div key={'title'}>size:<br /></div>)
            for ( let i in obj.product_variations){
                arr2.push( <div key={i}>RUS {obj.product_variations[i].brand_size}  (INT {obj.product_variations[i].uni_size} ) </div> )
            }
            SizeList = ()=> {return arr2}
        }

        return(
            <div className={product_cart_container_class}>
                {obj._id}
                <div className='close'>
                    <a onClick={() => this.props.prod_card_toggle() }> x </a>
                </div>
                <div className="gallery">
                    <ImgList />
                </div>
                {obj.name} {obj.brand_name} <br />
                price : {obj.price}<br />

                description: {obj.description}<br />

                <SizeList />
            </div>
        )}
}
export default {
    Categories_filter : Categories_filter_generate_list,
    ProductList : ProductListView,
    ProductCard : ProductCardGen
};