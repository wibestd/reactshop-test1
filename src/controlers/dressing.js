import React from 'react';

const Product = function (props) {
    let obj = props.obj
    let sizelist = props.sizelist
    console.log(obj);
    let arr =[]
    for (let p in sizelist )
        for ( let i in obj.product_variations){
            if (obj.product_variations[i].uni_size === sizelist[p])
                arr.push( <div key={i}>RUS {obj.product_variations[i].brand_size}  (INT {obj.product_variations[i].uni_size} ) </div> )
    }
    console.log(obj.product_variations);
    console.log(sizelist);
    console.log(arr);
    return (
        <div className='product'>
            <img src={obj.images_full} alt=""/>
            <div>
                Добавить в корзину
            </div>
            <div>
                {obj.name} {obj.brand_name}
            </div>
            {obj.id}
        </div>
    )
}

const ListOfCategory = function(props){
    let dressing_list= props.dressing_list
    let products_list= props.products_list
    let category_id = props.category_id
    console.log(dressing_list);
    console.log(products_list);
    console.log(category_id);
    let arr=[]
    for( let i in dressing_list ){
        if (dressing_list[i].status === category_id){
            let obj = products_list[ dressing_list[i].id ]
            arr.push( <Product key={obj.id} obj={obj} sizelist={dressing_list.size} /> )
            // arr.push( <div key={dressing_list[i].id}> {dressing_list[i].id} </div> )
        }
    }
    return arr
}

const Await_category_generate = function (props) {
    return (
        <div className='dressing_category'>
            <div className='title'> Заказано на примерку </div>
            <ListOfCategory category_id={1} dressing_list={props.dressing_list} products_list={props.products_list} />
        </div>
    )}
const In_dressing_category_generate = function (props) {
    return (
        <div className='dressing_category'>
            <div className='title'> Вещей на примерке </div>
            <ListOfCategory category_id={2} dressing_list={props.dressing_list} products_list={props.products_list} />
        </div>
    )}
const Cart_category_generate = function (props) {
    return (
        <div className='dressing_category'>
            <div className='title'> Вещей в корзине </div>
            <ListOfCategory category_id={3} dressing_list={props.dressing_list} products_list={props.products_list} />
        </div>
    )}

export default {
    Await_category: Await_category_generate,
    In_dressing_category: In_dressing_category_generate,
    Cart_category: Cart_category_generate
};