import React from 'react';
import {BrowserRouter, Route, NavLink} from "react-router-dom";

import Menu from "./view/menu";
import Cart from "./view/cart";
import Catalog from "./view/catalog";
import Dressing from "./view/dressing";

const {PraxComponent, byPath} = require('prax')
const {Atom} = require('espo') // transitive dependency
const {putIn} = require('emerge') // transitive dependency


const state = new Atom(
    {
        cart: {count_product_in_cart: 6},
        dressing: [
            {
                id:746786,
                status:1,
                size:[36,38],
            },{
                id:744366,
                status:2
            },{
                id:746790,
                status:3
            }
        ]

    });
class App extends PraxComponent {

    subrender ({deref}) {
        let handleClick = function() {
            let count_product_in_cart = deref(byPath(state, ['cart', 'count_product_in_cart']))
            state.swap(putIn, ['cart', 'count_product_in_cart'], count_product_in_cart+1)
        }

        let Start = function() { return <div> <NavLink activeClassName='active' to={'/catalog'} > start </NavLink> </div> }
        const msg = deref(byPath(state, ['cart', 'count_product_in_cart']))
        const dressing_list = deref(byPath(state, ['dressing']))

        return (
              <BrowserRouter >
                  <div className="App">
                      <Menu count_product_in_cart={msg} />
                      <main>
                          <Route exact path="/" component={Start} />
                          <Route path="/dressing" component={()=><Dressing dressing_list={dressing_list}/>} />
                          <Route path="/catalog" component={()=><Catalog />} />
                          <Route path="/cart" component={()=><Cart pp={handleClick} />}/>
                      </main>
                  </div>
              </BrowserRouter>
        );
    }
}

export default App;
