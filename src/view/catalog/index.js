import React from 'react';
import GenderFilter from "./Gender_filter";
import AsideBlock from "./Aside_block";
import ProductsContainer from "./ProductsContainer";

const {Api} = require('../../controlers')

const {PraxComponent, byPath} = require('prax')
const {Atom} = require('espo') // transitive dependency
const {putIn} = require('emerge') // transitive dependency

const state = new Atom(
    {
        catalog: {
            tree: ['женская одежда', 4],
            filters: ['женская одежда', 4],
            loader_classname: 'loader',
            products_obj: {}
        },
        categories: {
            obj: {}
        },
        non_hide_loader: {
            non_hide_loader_classname: 'hide'
        },
        product_card_obj: {
            isactive: false,
            current_obj: {empty: true}
        }
    });

class Index extends PraxComponent {
    subrender({deref}) {
        console.log( '=====>>>>>>>>>>>' )
        console.log('CATALOG')

        let categories_obj = deref(byPath(state, ['categories', 'obj']));
        let categories_tree = deref(byPath(state, ['catalog', 'tree']))
        // wait categories_obj
        // console.log( '_______WHOLE RENDER' ); console.log( '______load_categories_obj init' );
        if (!categories_obj[0]) {  load_categories_obj(); return <div className={'container'}><div className={'middle_container'}><div className={'loader'}><img src="/img/loader.png" alt=""/></div></div></div>;}

        let products_obj = deref(byPath(state, ['catalog', 'products_obj']));
        let filters_tree = deref(byPath(state, ['catalog', 'filters']))
        let filters_obj = products_obj.filters_new;
        // wait products_obj
        // console.log( '_______WHOLE RENDER' ); console.log( '______load_products_obj init' );
        if (!products_obj.data) { load_products_obj( categories_tree[1] ); return <div className={'container'}><GenderFilter categories_tree={ categories_tree } change_categories_tree={ change_categories_tree }/><div className={'middle_container'}><div className={'loader'}><img src="/img/loader.png" alt=""/></div></div></div>;}

        // console.log( '_______CATALOG SUCCESS' )

        let loader_classname = deref(byPath(state, ['catalog', 'loader_classname']))
        let non_hide_loader_classname = deref(byPath(state, ['non_hide_loader', 'non_hide_loader_classname']))
        let gender_id = categories_tree[1];
        let product_card_obj = deref(byPath(state, ['product_card_obj']))

        if (gender_id === 4) categories_obj = categories_obj[ 0 ].subcategories;
        if (gender_id === 5) categories_obj = categories_obj[ 1 ].subcategories;
        console.log('categories_tree', categories_tree)
        console.log('categories_obj', categories_obj)
        console.log('products_obj', products_obj)

        function change_categories_tree(array) {
            console.log( 'change_categories_tree' );
            let obj = deref(byPath(state, ['catalog']) )
            state.swap(putIn, ['catalog'], {
                tree:array,
                filters: obj.filters,
                loader_classname:'loader',
                products_obj: {},
            })
        }
        function loaderON() { console.log('loaderON');  state.swap(putIn, ['catalog', 'loader_classname'], 'loader') }
        function loaderOff() { console.log('loaderOff'); state.swap(putIn, ['catalog', 'loader_classname'], 'hide') }
        function non_hide_load(array) {
            const id = array[ array.length-1 ]
            console.log( '_______UPLOAD '+id+' cat' )
            Api.getCatalogListObj( id ).then( yyy => {
                console.log( '_______PRODUCTS UPLOADED' )
                console.log( '_______STARTING UPDATE...' )
                if (yyy === 'error') { alert("api don't work"); return;}
                state.swap(putIn, ['catalog'], {
                    tree: array,
                    loader_classname: 'hide',
                    products_obj: yyy
                })
            });
        }
        function load_categories_obj() {
            Api.getCategoryObj().then( yyy => {
                console.log( '_______CATS UPLOAD' )
                console.log( '_______STARTING UPDATE...' )
                if (yyy === 'error') { alert("api don't work"); return;}
                state.swap(putIn, ['categories', 'obj'], yyy)
            });
        }
        function load_products_obj(id) {
            Api.getCatalogListObj( id ).then( yyy => {
                console.log( '_______PRODUCTS UPLOADED' )
                console.log( '_______STARTING UPDATE...' )
                if (yyy === 'error') { alert("api don't work"); return;}
                loaderOff()
                state.swap(putIn, ['catalog', 'products_obj'], yyy)
            });
        }
        function categories_block_onclick(array) {
            loaderON()
            non_hide_load(array)
        }
        function prod_card_toggle(obj) {
            let wew = deref(byPath(state, ['product_card_obj', 'isactive']) )
            console.log(obj);
            state.swap(putIn, ['product_card_obj'],{
                isactive: !wew,
                current_obj: obj
            });

        }
        return (
            <div className={'container'}>
                <GenderFilter categories_tree={ categories_tree } change_categories_tree={ change_categories_tree } />
                <div className={'middle_container'}>
                    <div className={loader_classname}>
                        <img src="/img/loader.png" alt=""/>
                    </div>
                    <div className={non_hide_loader_classname}>
                        <img src="/img/loader.png" alt=""/>
                    </div>
                    <AsideBlock categories_tree={categories_tree} categories_obj={categories_obj} filters_obj={filters_obj} filters_tree={filters_tree} categories_block_onclick={categories_block_onclick} />
                    <ProductsContainer categories_tree={categories_tree} products_obj={products_obj} product_card_obj={product_card_obj} prod_card_toggle={prod_card_toggle}/>
                </div>
            </div>
        );
    }
}



export default Index;