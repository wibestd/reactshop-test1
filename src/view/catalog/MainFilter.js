import React from 'react';

// const {Categories} = require('../../controlers')

const {PraxComponent, byPath} = require('prax')
const {Atom} = require('espo') // transitive dependency
const {putIn} = require('emerge') // transitive dependency

const Angle = () => (
    <div className={'main_filter_angle'}>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"/></svg>
    </div>
)

const state = new Atom(
    {
        price: false,
        price_val: false,
        discount:false,
        size:false,

    }
);

class Main_filter extends PraxComponent {
    // constructor(props){
    //     super(props);
    //
    // }

    subrender({deref}) {
        console.log('Main_filter');
        let result = [], filter_id

        let filters_obj = this.props.filters_obj
        // let filters_tree = this.props.filters_tree
        let name

        console.log(filters_obj);
            // console.log(filter);

        let price = (obj, is_active) => (
            <div className={(is_active)?'subcontainer show':'subcontainer hide' }>
                Цена от {obj.value_from} до {obj.value_to}
                <div className="price_input">
                    <input type="text" /> <input type="button" value="Apply" />
                </div>
            </div>
        ),
        brand_id = (obj, is_active) =>{
            if (obj.values.length<4) return <div></div> ;
            return(
            <div className={(is_active)?'subcontainer show':'subcontainer hide' }>
                {obj.values.length} шт
                <div className="price_input">
                    <input type="checkbox" value={obj.values[0].value} id={obj.values[0].id} /> {obj.values[0].name} <br />
                    <input type="checkbox" value={obj.values[1].value} id={obj.values[1].id} /> {obj.values[1].name} <br />
                    <input type="checkbox" value={obj.values[2].value} id={obj.values[2].id} /> {obj.values[2].name} <br />
                    <input type="checkbox" value={obj.values[3].value} id={obj.values[3].id} /> {obj.values[3].name} <br />
                </div>
            </div>
        )},
        discount = (obj, is_active) =>{
            if (obj.values.length<4) return <div></div> ;
            return(
            <div className={(is_active)?'subcontainer show':'subcontainer hide' }>
                <div className="price_input">
                    <input type="checkbox" value={obj.values[0].value} id={obj.values[0].id} /> {obj.values[0].name} <br />
                    <input type="checkbox" value={obj.values[1].value} id={obj.values[1].id} /> {obj.values[1].name} <br />
                    <input type="checkbox" value={obj.values[2].value} id={obj.values[2].id} /> {obj.values[2].name} <br />
                    <input type="checkbox" value={obj.values[3].value} id={obj.values[3].id} /> {obj.values[3].name} <br />
                </div>
            </div>
        )},
        collection_id = (obj, is_active) => {
            if (obj.values.length<4) return <div className={(is_active)?'subcontainer show':'subcontainer hide' }>no collection filters (</div> ;
            return (
            <div className={(is_active)?'subcontainer show':'subcontainer hide' }>
                ...
            </div>
        )},
        color_id = (obj, is_active) =>{
        if (obj.values.length<4) { console.log(obj); return <div></div> ;}
        return(
            <div className={(is_active)?'subcontainer show':'subcontainer hide' }>
                <div className="price_input">
                    <input type="checkbox" value={obj.values[0].value} id={obj.values[0].id} /> {obj.values[0].name} <br />
                    <input type="checkbox" value={obj.values[1].value} id={obj.values[1].id} /> {obj.values[1].name} <br />
                    <input type="checkbox" value={obj.values[2].value} id={obj.values[2].id} /> {obj.values[2].name} <br />
                    <input type="checkbox" value={obj.values[3].value} id={obj.values[3].id} /> {obj.values[3].name} <br />
                </div>
            </div>
        )};
        name = 'brand_id'
        for ( let i in filters_obj ){if (filters_obj[i].param_name !== name ) continue;
            result.push( template_filter_block( filters_obj[i], deref(byPath(state, [name]) ),name, brand_id ));
        }
        name = 'size'; filter_id = 21;
        for ( let i in filters_obj ){if (filters_obj[i].id !== filter_id ) continue;
            result.push( template_filter_block( filters_obj[i], deref(byPath(state, [name]) ),name, price ));
        }
        name = 'discount'
        for ( let i in filters_obj ){if (filters_obj[i].param_name !== name ) continue;
            result.push( template_filter_block( filters_obj[i], deref(byPath(state, [name]) ),name, discount ));
        }
        name = 'collection_id'
        for ( let i in filters_obj ){if (filters_obj[i].param_name !== name ) continue;
            result.push( template_filter_block( filters_obj[i], deref(byPath(state, [name]) ),name, collection_id ));
        }
        name = 'color_id'
        for ( let i in filters_obj ){if (filters_obj[i].param_name !== name ) continue;
            result.push( template_filter_block( filters_obj[i], deref(byPath(state, [name]) ),name, color_id ));
        }
        name = 'price'
        for ( let i in filters_obj ){if (filters_obj[i].param_name !== name ) continue;
            result.push( template_filter_block( filters_obj[i], deref(byPath(state, [name]) ),name, price ));
        }
        function template_filter_block(obj, is_active, name, currentFilter) {
            let CurrentFilter= () => currentFilter(obj, is_active);
            return (
                 <div key={obj.id} className={'main_filter_block'}>
                     <div className={'href_container'} onClick={ ()=>{ state.swap(putIn, [name], !is_active)  } } >
                         <Angle />
                         <a> {obj.name} </a>
                     </div>
                     <CurrentFilter />
                 </div>
            )
        }

        return result;
    }
}


export default Main_filter;