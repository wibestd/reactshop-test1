import React from 'react';

import MainFilter from "./MainFilter";

const {PraxComponent} = require('prax')
const {Catalog} = require('../../controlers')

class Aside_block extends PraxComponent {
    subrender({deref}) {
        let CategoriesFilter = Catalog.default.Categories_filter;

        console.log('ASIDE OK')
        return (
            <aside>
                <div className={'main_filter_container'}>
                    <MainFilter categories_obj={this.props.categories_obj} filters_obj={this.props.filters_obj} filters_tree={this.props.filters_tree} />
                </div>
                <div className={'category_filter_container'}>
                    <CategoriesFilter categories_block_onclick={this.props.categories_block_onclick} categories_tree={this.props.categories_tree} categories_obj={this.props.categories_obj}  />
                </div>
            </aside>
        );
    }
}

export default Aside_block;