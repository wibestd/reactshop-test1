import React, { Component } from 'react';

class Info_line extends Component {
    render() {
        const select_list=[

        ]
        return (
            <div className={this.props.info_line_class}>
                <div className={'left'}>
                    <div className="category">{this.props.category_of_select}</div>
                    <div className="count_in_selection">{this.props.count_of_select} товаров</div>
                </div>
                <div className={'sort_select_container'}>
                    <div className={'show_block'}>
                        <div className={'text'}>
                            По популярности
                        </div>
                        <div className={'angle_down'}>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M143 352.3L7 216.3c-9.4-9.4-9.4-24.6 0-33.9l22.6-22.6c9.4-9.4 24.6-9.4 33.9 0l96.4 96.4 96.4-96.4c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9l-136 136c-9.2 9.4-24.4 9.4-33.8 0z"/></svg>
                        </div>
                    </div>
                    <div className={'dropdown'}>

                    </div>
                </div>
            </div>
        );
    }
}

export default Info_line;