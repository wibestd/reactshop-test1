import React from 'react';

const {PraxComponent} = require('prax')

class Gender_filter extends PraxComponent {
    subrender({deref}) {
        let gender_tree_id = this.props.categories_tree[1]

        let link1_class = gender_tree_id === 4 ? 'active' : 'n'
        let link2_class = gender_tree_id=== 5 ? 'active' : 'n'

        console.log('GENDER OK')
        return (
            <div className={'gender_filter_block'}>
                <ul>
                    <li>
                        <a className={link1_class} onClick={ ()=>{ this.props.change_categories_tree( ['женская одежда', 4] ) } } >Женщинам</a>
                    </li>
                    <li>
                        <a className={link2_class} onClick={ ()=>{ this.props.change_categories_tree( ['мужская одежда', 5] ) } } >Мужчинам</a>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Gender_filter;