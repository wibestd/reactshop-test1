import React  from 'react';
import InfoLine from "./InfoLine";
import {Catalog} from "../../controlers";

const {PraxComponent} = require('prax')


class Products_container extends PraxComponent {
    subrender({deref}) {
        console.log('PRODUCTS_CONT')

        const ProductList = Catalog.default.ProductList;
        const ProductCard = Catalog.default.ProductCard;

        const products_container_class = (!this.props.product_card_obj.isactive) ? 'products_container' : 'hide'
        const info_line_class = (!this.props.product_card_obj.isactive) ? 'info_line' : 'hide'

        console.log({ProductList, ProductCard});
        console.log('PRODUCTS_CONTAINER END')

        return (
            <div className='center_container'>
                <ProductCard product_card_obj={this.props.product_card_obj} prod_card_toggle={this.props.prod_card_toggle} />
                <InfoLine category_of_select={this.props.categories_tree[0]} info_line_class={info_line_class}  count_of_select={this.props.products_obj.total.toLocaleString('ru') }/>
                <div className={products_container_class} >
                    <ProductList prod_card_toggle={this.props.prod_card_toggle} products_obj={this.props.products_obj}  />
                </div>
            </div>
        );
    }
}

export default Products_container;

