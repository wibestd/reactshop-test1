import React, { Component } from 'react';
import {NavLink} from 'react-router-dom'

class Main_link extends Component {
    render() {
        return (
            <li>
                <NavLink activeClassName='active' to={this.props.to} >
                    <img src={this.props.pic} alt="Примерка"/>
                    <span>{this.props.text}</span>
                </NavLink>
            </li>
        );
    }
}

export default Main_link;