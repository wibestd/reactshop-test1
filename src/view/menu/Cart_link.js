import React, { Component } from 'react';
import {NavLink} from 'react-router-dom'

class Main_link extends Component {
    render() {
        return (
            <li>
                <NavLink activeClassName='active' to="/cart" >
                    <div className="cart_cont">

                        <div className="count_product_in_cart_container">
                            <div className="count_product_in_cart_sub_container">
                                {this.props.count_product_in_cart}
                            </div>
                        </div>
                        <img src={this.props.pic} alt="Корзина"/>
                    </div>
                    <span>Корзина</span>

                </NavLink>
            </li>
        );
    }
}

export default Main_link;