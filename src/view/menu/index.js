import React, { Component } from 'react';

import MainLink from "./Main_link";
import CartLink from "./Cart_link";
import Staff from "./Staff";
class Index extends Component {
    render() {
        return (
            <nav className="mainmenu">
                <div className="nav_border"/>
                <Staff />
                <ul>
                    <MainLink to={'/dressing'} pic={'/img/mainmenu/menu_pic_dressing.png'} text={'Примерка'}/>
                    <MainLink to={'/catalog'} pic={'/img/mainmenu/menu_pic_catalog.png'}  text={'Каталог'}/>
                    <CartLink to={'/dressing'} pic={'/img/mainmenu/menu_pic_cart.png'} count_product_in_cart={this.props.count_product_in_cart} />
                </ul>
            </nav>
        );
    }
}

export default Index;