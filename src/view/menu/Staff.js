import React, { Component } from 'react';

class Staff extends Component {
    render() {
        return (
            <div className={'staff'}>
                <img src={"img/staff.jpeg"} alt={""} />
                <div className={'right_side'}>
                    <div className={'bell'}><img src="img/bell.jpeg" alt=""/> </div>
                    <div className={'name'}> Алёна </div>
                </div>
            </div>
        );
    }
}

export default Staff;