import React  from 'react';

const {Api, Dressing} = require('../../controlers')

const {PraxComponent, byPath} = require('prax')
const {Atom} = require('espo') // transitive dependency
const {putIn} = require('emerge') // transitive dependency

const state = new Atom(
    {
        dressing: {
            tree: ['женская одежда', 4],
            loader_classname: 'loader',
            products_list: []
        }
    });

// const {PraxComponent} = require('prax')
class Index extends PraxComponent {
    subrender({deref}) {
        console.log('start')
        let dressing_list = this.props.dressing_list
        let global_dressing_count = dressing_list.length, k=0
        let products_list = deref(byPath(state, ['dressing', 'products_list']));
        
        const AwaitCategory = Dressing.default.Await_category;
        const InDressingCategory = Dressing.default.In_dressing_category;
        const Cart_category = Dressing.default.Cart_category;

        let loader_classname = deref(byPath(state, ['dressing', 'loader_classname']))
        let loaded_product_count=0;
        for (let i in products_list) loaded_product_count++;

        if (loaded_product_count!==global_dressing_count) { console.log('clear'); dressing_build(); return (<div className={loader_classname}>            <img src="/img/loader.png" alt=""/>        </div>) }
        
        function dressing_build() {
            for ( let i in dressing_list ){
                // console.log(dressing_list)
                product_obj_load( dressing_list[i].id )
            }
        }
        
        function product_obj_load(id) {
            Api.getProductObj(id).then( obj => {
                products_list[obj.id]=obj;
                k++;

                if (global_dressing_count === k) {
                    state.swap(putIn, ['dressing'], {
                        tree:['женская одежда', 4],
                        loader_classname:'hide',
                        products_list: products_list,
                    })
                }
            })
        }

        return (
            <div>
                <div className={loader_classname}>
                    <img src="/img/loader.png" alt=""/>
                </div>
                <AwaitCategory dressing_list={dressing_list} products_list={products_list} />
                <InDressingCategory dressing_list={dressing_list} products_list={products_list} />
                <Cart_category dressing_list={dressing_list} products_list={products_list} />
            </div>
        );
    }
}



export default Index;