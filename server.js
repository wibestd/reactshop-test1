let express = require('express');
let request = require('request');
let iconv  = require('iconv-lite');

// let fs = require("fs");
// let test_cat_json = fs.readFileSync("cat.json");
// test_cat_json = JSON.parse(test_cat_json);
let app = express()

app.get('/categories', async function (req, resp) {
    let opt = {
        url: 'http://dds6.butik.ru/api/categories',
        encoding: null
    }
    request(opt, function (err, res, body) {
        if (err) throw err;
        // console.log(iconv.decode(body, 'win1251'));
        console.log(`data from "${opt.url}" received :)`);
        resp.setHeader('Access-Control-Allow-Origin', '*');
        resp.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');
        resp.send( iconv.decode(body, 'win1251') )
    });
})

app.get('/catalog/:categoryId', async function (req, resp) {
    let opt = {
        url: 'http://dds6.butik.ru/api/products?category_id='+req.params.categoryId,
        encoding: null
    }
    request(opt, function (err, res, body) {
        if (err) throw err;
        // console.log(iconv.decode(body, 'win1251'));
        console.log(`data from "${opt.url}" received :)`);
        resp.setHeader('Access-Control-Allow-Origin', '*');
        resp.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');
        resp.send( iconv.decode(body, 'win1251') )
    });
})

app.get('/product/:productId', async function (req, resp) {
    let opt = {
        url: 'http://dds6.butik.ru/api/product/'+req.params.productId,
        encoding: null
    }
    request(opt, function (err, res, body) {
        if (err) throw err;
        // console.log(iconv.decode(body, 'win1251'));
        console.log(`data from "${opt.url}" received :)`);
        resp.setHeader('Access-Control-Allow-Origin', '*');
        resp.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');
        resp.send( iconv.decode(body, 'win1251') )
    });
})
app.listen(3003)